<?php
function tentukan_nilai($number)
{
    if (85<=$number && $number<=100){
        echo "Sangat Baik";
    } else if (70<=$number && $number<=80){
        echo "Baik";
    } else if (60<=$number && $number<=70){
        echo "Cukup";
    }else {
        echo "Kurang";
    }
    echo "<br>";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>