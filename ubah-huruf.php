<?php
function ubah_huruf($string)
{
    $panjangString = strlen($string);
    $nextLetter = "";
    for ($i = 0; $i < $panjangString; $i++) {
        $letter = $string[$i];
        $nextLetter .=  ++$letter;
    }
    echo $nextLetter;
    echo "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
